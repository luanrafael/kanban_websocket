from flask import Flask, request,abort,json
from flask_socketio import SocketIO, send, emit,join_room, leave_room

app = Flask(__name__)
app.debug = True
app.config['SECRET_KEY'] = 'pwduyabbYGAffaA'
socketio = SocketIO(app)

@app.route("/")
def home():
        return "ok"

@app.route('/emit',methods=['POST'])
def send_emit():
    if request.method != 'POST':
        abort(403)
    if request.data is None:
        abort(403)
    data = request.get_json()
    action = data['action']
    room_data = 1

    message_actions = ['new', 'current', 'edit']
    if action in message_actions:
        action = 'message'

    socketio.emit(action,data,room=room_data)
    return json.dumps({'message':'ok'})


@socketio.on('join')
def on_join(data):
    room = 1
    join_room(room)
    #send(' has entered the room.', room=room)

@socketio.on('leave')
def on_leave(data):
    username = data['username']
    room = data['room']
    leave_room(room)
    send(username + ' has left the room.', room=room)

@socketio.on('connect')
def test_connect():
    emit('+ 1', {'data': 'Connected'},broadcast=True)

@socketio.on('disconnect')
def test_disconnect():
    print('Client disconnected')

@socketio.on('new_message')
def on_info(data):
    emit('info',data,room=1)

@socketio.on('kanban_action')
def on_message(data):
    emit('message',data, room=1,include_self=False)