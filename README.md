# README #

### 1 - Python 2.7 ###
Instalar a versão 2.7 do Python
https://www.python.org/download/releases/2.7/

### 2 - Python Pip ###
Instalar o Python Pip
https://pip.pypa.io/en/stable/ ou https://bootstrap.pypa.io/get-pip.py

### 3 - Flask Socket IO ###
Instalar via **pip** as dependências do projeto.


```
#!cmd

pip install flask-socketio
```

### 4 - Executar o arquivo run.py ###


```
#!cmd
python run.py

```


obs. Para testar usando o kanban é preciso altera o IP no arquivo **socket2.js** e **WebSocketController** pois ainda não configurei para pegar o endereço do arquivo **.env**